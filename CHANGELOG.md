Versions
========

- Major revision change can break backward compatibility.
- Minor revision correspond to an addition of functionality.
- Patch revision: small changes and bugfixes (should not be too much given the size of the project).

v3.1.0 [7f836d65efa31fda508c1229e19750a4813c80ae]
- New functionality:
  - Add master option for `xmin/xmax`, with `auto` default.

v3.0.0 [59984db43c8c8963d47ec7c6140951831607eff5]
- Major interface change:
  - Allow `auto` keyword for `ymin/ymax`.
  - Removed keywords: `yAxisAuto`, `yAxisAutoMin`, `yAxisAutoMax`.

v2.8.0 [4ef28f1fba4934facc71fdea34a1610d05ea6ef0]
- New functionality:
  - CLI can be provided with several input config files.
- Cleaning:
  - pythonic way for attributes.

v2.7.0 [509bd58c106b8dcbc82243438a022af5f1b3b732]
- New functionality:
  - `inputFile` accept several values (coma-separated list): builds a TChain from them before
    projecting. For reading, only the first file is taken into account.
- Internal changes:
  - Preparatory work for above (stringToList returned typed values, other).

v2.6.2 [6bc9bc4f392a73339709578d529be44b7157fd43]
- Interface change (minor):
  - Add text line 4 and 5 in TextFormatter.

v2.6.1 [cbc89b247ee8d5942dc2796988bbdf3e995b5329]
- Internal change:
  - Draw ATLAS label even if ATLASStyle is not set but ATLASLabel is not void:
    allow for "ATLAS" if "ATLASStyle = True", and "ATLAS Internal" with only "ATLASLabel = Internal".

v2.6.0 [a87d85faba9700aeaa56fc28c5ca5733bf87436d]
- New colors available for H4l plots (defined starting at 1179).

v2.5.4 [b1155d3144d76de09e2290063b5c76567a20b86d]
- Bugfixes:
    - Correct FillStyle:
        - solid white = `fillStyle=1001`
        - other, just change whatever needed.
        - default is no fill
    - Boxes (text and legend) alpha set to 0.

v2.5.3 [2a68b1b8bc017696074a1a81dd16aaaef7ae5161]
- Bugfixes:
    - Hijacking of CLI options by ROOT

v2.5.2 [8fc4a9efa54056166c4708feb3c8b60bf110ef5f]
- Bugfixes:
    - Do not normalize when integral is 0.

v2.5.1 [b322b67fdfe7c975c0a86be435e6bd306736b9d7]
- Bugfixes:
    - Segfault when no ratio pad required.
- Internal changes:
    - Naming conventions for name/path/dir...
    - GetSumOfWeights instead of Integral.

v2.5.0 [cd6302f32320a92bc4606f03700713656f59613a]
- New interface options:
    - `binningX`, `binningY`: allow for irregular binning on both axes (coma-separated).

v2.4.1 [787b4aa602e9a1d3a650577235bb35f357fff163]
- RootPlotCombiner is executable.
- Internal changes:
    - Updated docstrings.

v2.4.0 [681b435c6f520199308610c78236e095621318a3]
- New interface for text boxes.

v2.3.1 [7bff6901285bebeb6ce4b39fae4ac94562be3b67]
- Add option to specifically print internal maps.

v2.3.0 [b4332061f0d348ce38f4b287fd2133702e41abea]
- Internal changes:
    - Renamed RootPlotCombiner to MasterCombiner.
    - RootPlotCombiner still is main exe.
    - Added command line setup.

v2.2.2 [b647c4f42b7b1daff2c2e2862297fa0caf7a85bc]
- Bugfix:
    - auto ymax/ymin based on main pad only (not ratio pad).

v2.2.1 [65c3af42577011dda3a52af98711a29bc4d8b988]:
- Internal change:
    - Correct internal name conflict of sections.

v2.2.0 [a26f846b828ba0f4b6ae04145dd5c819931e2821]:
- Interface change:
    - Rename option `botMargin` -> `bottomMargin`.
- New interface options:
    - `canvSizeX`, `canvSizeY`, default 800, 600.
    - `legendTextSize`, default 0.05

v2.1.0 [825cdc173d8b80b85b6d8a70e663c5bcf090fe08]:
- New interface options:
    - possibility to add a ratio panel.
- Internal changes:
    - formatting (PyLint).
    - option map is defined in external function.
    - internal variables prefixed with _

v2.0.0 [d0dd7c5ddd8977df82151e5c9881f5a374a2677c]: **Backward compatibility break.**
- Moved to description content/format in config files.
- First fully functional v2.

v1.8.0 [f4bdd365355e5e185898e8428916d9cf790a0f30]:
- New interface options:
    -  can shift histograms in x-axis to avoid close superimposition.

v1.7.3 [a4f271a66b07029eb0d650bdb36157a333ae3233]:
- Internal changes:
    - Using `ConfigParser` instead of custom parser.
    - `optionMap` definition moved to function.
    - Update unit_test for `Helpers.py`

v1.7.2 [8520e4734b47da0088899e0e630b78f8954a48ba]:
- Bugfixes:
    - Added test suite for `ReaderHelpers.py`.

v1.7.1 [515da9af84fa1f0b245f561792e2a3167fd4f532]:
- Internal changes:
    - options container type changed from `Dict` to `OrderedDict`.
    - comply with `__future__`.
    - boolean cast using `distutils.util.strtobool`.

v1.7.0 [c3c1a4eb0b37ad359519b5e6d390306abeef3746]:
- Interface changes:
    - output name now optional (build from config file name).

v1.6.0 [a10cfc9214f228399061c575192883da953c303c]:
- New interface options:
    - possibility to save all internal histograms in a ROOT file.
- Bugfixes:
    - `1DProfile` option correctly recognized in section header.
- Internal changes:
    - internal hist names not dummy but as retrieved

v1.5.0 [445786cafdf0cb26892f211b6e1eb59abe38cc5f]:
- New interface options:
    - ATLAS Style
    - ATLAS labels
- Internal changes
    - Style application moved to functions.

v1.4.2 [fad9ef88edda16432e09bb834195f1c77097e03d]:
- Bugfixes:
    - Normalization for divided histogram done before division.

v1.4.1 [7c04e79cdfc4027eaa04460882751786fcb2cd7c]:
- Output impact:
    - Add some styling corresponding to ATLAS.

v1.4.0 [d1cb4f58d017019d4f4837e9b3f67f05f4938bd5]:
- New interface options:
    - Added possibility to on the fly subtract and/or divide histograms.

v1.3.0 [817771f4e77d4cff8468241587239c6f7e405117]:
- New interface options:
    - Auto range of yAxis can be used separately for min and max.

v1.2.2 [750011dc840adaed43236ad06c97f74187caa822]:
- Output impact:
    - Small change in the legend box and line size.

v1.2.1 [bd7b478f38b568aa3e774fb79ef6d0e75fbc674e]:
- Bugfixes:
    - Spelling mistake.
    - Added comments and debug info.

v1.2.0 [7509e5ce193efaf660d1594eacae7b4ac0759a71]:
- New interface options:
    - Can now use profiles in projection.

v1.1.0 [4920382f9b97e95d8e91ed2dcfa4ca52ae6da930]:
- New interface options:
    - Added grid display option.

v1.0.1 [0f182b2e5ebd15886a333199f848cb083cfda7c2]:
- Output impact:
    - Added small space at the top/bottom of hists.
- Bugfixes:
    - Small correction to the histogram type recognition.
- Internal changes
    - Using dedicated printing functions in all classes.
    - Making sure to use `sys.exit()` instead of `exit()`.

v1.0.0 [8851c94a0a9758c9f24cb840091b50c29fc16380]:
- First fully working version.


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


TODO-list:
==========

- Doxygen

- Add date of production somewhere (removed with atlas style)
- unit test (do not store root file, generate it on the fly): https://docs.python.org/2.7/library/unittest.html
  -> partially done...
- cleanup before closing (or is it automatically handle by garbage collector? not sure of interaction with ROOT objects)
- Mix boundaries for legend and atlas label position (left OR right + top OR bottom corner)
- add legend box auto sizing (auto choice if height exceed some value?)
- option to split legend in two columns if needed

- Emit WARNING if two sections have the same name.
- Default output -> pdf + C


Ideas
-----

- option could be dictionary (or Option class or named tuple): `key: {value, type, allowMultiple}`
- Store if option is default or has been changed -> why?

At the moment only two pads are allowed: master and ratioPanel (`plotOn` option).
If more are needed, one could add other section category (along with `hist` and `plot`)
describing a pad (axes names, offset, range, log, using grid)
while master would only describe the style and saving options of the plot, and the legend properties.

