#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date Aug 2018
@file ut_Helpers.py
@brief Test module for RootPlotCombiner utils.
"""
from __future__ import print_function, division, absolute_import, unicode_literals

import unittest
from collections import OrderedDict
import ConfigParser
import StringIO
from Helpers import readConfigFile, mergeDicts


class TestReadConfigFileBase(unittest.TestCase):
    def outputMsg(self, comment):
        msg = comment + '\n'
        for line in self.inputLines:
            msg += "input : '{}'\n".format(line)
        msg += "result: {}\n".format(self.optionMap)
        msg += "expect: {}\n".format(self.expectedResult)
        return msg
        
    def prepareInput(self):
        ret = StringIO.StringIO()
        if not isinstance(self.inputLines, list):
            self.inputLines = self.inputLines.split('\n')
        for line in self.inputLines:
            ret.write(line + '\n')
        ret.seek(0) # Otherwise cursor stays as the end when reading.
        return ret



class TestReadConfigFile_emptyLine(TestReadConfigFileBase):
    def setUp(self):
        self.inputLines = ["[master]"]
        self.expectedResult = OrderedDict({'master': {}})

    def test_commentedLine(self):
        self.inputLines += ["# option = value2"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Commented line should not alter option map"))

    @unittest.skip("Lines starting with spaces will not be stripped anymore")
    def test_commentedLineWithSpace(self):
        self.inputLines += [" # option = value2 "]
        self.optionMap = readConfigFile(self.prepareInput())
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Commented line starting with space should not alter option map"))

    def test_emptyLine(self):
        self.inputLines += [""]
        self.optionMap = readConfigFile(self.prepareInput())
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Empty line should not alter option map"))

    def test_notEnoughTokens(self): # Should raise ConfigParser.ParsingError and fail.
        self.inputLines += ["a line without equal sign"]
        caughtError = False
        try: self.optionMap = readConfigFile(self.prepareInput())
        except (ConfigParser.ParsingError, IndexError): caughtError = True
        self.assertTrue(caughtError, "Line without = should raise an error ({})".format(self.inputLines))

    def test_allowNoValue(self): # Should raise ConfigParser.ParsingError and fail
        self.inputLines += ["option"]
        caughtError = False
        try: self.optionMap = readConfigFile(self.prepareInput())
        except (ConfigParser.ParsingError, IndexError): caughtError = True
        self.assertTrue(caughtError, "Option without equal sign should not be recognized ({})".format(self.inputLines))



class TestReadConfigFile_lineFormatting(TestReadConfigFileBase):
    def setUp(self):
        self.inputLines = ["[master]"]

    @unittest.skip("Multiline not implemented yet.")
    def test_multilineValue(self):
        pass

    @unittest.skip("Line starting with spaces: not stripped anymore, now used as multiline value (not implemented yet).")
    def test_spaceBeforeOption(self):
        self.inputLines += ["  option = another string"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {'option': 'another string'}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("option name was not lstrip'd"))

    def test_spaceAfterOption(self):
        self.inputLines += ["option    = another string"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {'option': 'another string'}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("option name was not rstrip'd"))

    def test_spaceBeforeValue(self):
        self.inputLines += ["option =      another string"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {'option': 'another string'}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("value was not lstrip'd"))

    def test_spaceAfterValue(self):
        self.inputLines += ["option = another string      "]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {'option': 'another string'}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("value was not rstrip'd"))

    def test_caseSensitiveOption(self):
        self.inputLines = ["[MasTeR]"]
        self.inputLines += ["CaseOption = CaSed StrinG"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'MasTeR': {'CaseOption': 'CaSed StrinG'}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Not conserving case in options"))

    def test_multipleSeparator(self):
        self.inputLines += ["option = title = 5 GeV"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {'option': 'title = 5 GeV'}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Wrongly split multiple separators"))

    def test_lineWithHashtag(self):
        self.inputLines += ["option = title #3"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {'option': 'title #3'}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Hashtag in value wrongly recognized as comment"))

    def test_twoWordOption(self):
        self.inputLines += ["two words = value"]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {"two words": "value"}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Two word option not recognized"))

    def test_allowEmptyValue(self):
        self.inputLines += ["option = "]
        self.optionMap = readConfigFile(self.prepareInput())
        self.expectedResult = OrderedDict({'master': {'option': ''}})
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Empty value should empty option map value"))


#___________________________________________________________________________
#___________________________________________________________________________
#___________________________________________________________________________


class TestMergeDictsBase(unittest.TestCase):
    def outputMsg(self, comment):
        msg = comment + '\n'
        msg += "input : {}\n".format(self.newOpts)
        msg += "result: {}\n".format(self.optionMap)
        msg += "expect: {}\n".format(self.expectedResult)
        return msg



class TestMergeDict_String(TestMergeDictsBase):
    def test_fromString(self):
        self.optionMap = {'option': 'some string'}
        self.newOpts = {'option': 'another string'}
        self.expectedResult = {'option': 'another string'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Could not replace option"))

    def test_unrecognizeOption(self):
        self.optionMap = {'option': 'a string'}
        self.newOpts = {'anotherOption': 'another string'}
        self.expectedResult = {'option': 'a string'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Unknown option should not have been recognized"))

    @unittest.skip("multitokens not implemented yet")
    def test_multitokens(self):
        pass



class TestMergeDicts_True(TestMergeDictsBase):
    def setUp(self):
        self.optionMap = {'option': False}
        self.expectedResult = {'option': True}

    def test_bool_1(self):
        self.newOpts = {'option': '1'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Should cast '1' to True"))

    def test_bool_true(self):
        self.newOpts = {'option': 'true'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Should cast 'true' to True"))

    def test_bool_True(self):
        self.newOpts = {'option': 'True'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Should cast 'True' to True"))



class TestMergeDicts_False(TestMergeDictsBase):
    def setUp(self):
        self.optionMap = {'option': True}
        self.expectedResult = {'option': False}

    def test_bool_0(self):
        self.newOpts = {'option': '0'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Should cast '0' to False"))

    def test_bool_false(self):
        self.newOpts = {'option': 'false'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Should cast 'false' to False"))

    def test_bool_False(self):
        self.newOpts = {'option': 'False'}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Should cast 'False' to False"))



class TestMergeDicts_Float(TestMergeDictsBase):
    def test_fromFloat(self):
        self.optionMap = {'option': 2.5}
        self.newOpts = {'option': '1.2'}
        self.expectedResult = {'option': 1.2}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Issue converting string to double"))

    def test_fromInt(self):
        self.optionMap = {'option': 2.5}
        self.newOpts = {'option': '1'}
        self.expectedResult = {'option': 1.}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("int representation should be cast to original type float"))



class TestMergeDicts_Int(TestMergeDictsBase):
    def test_fromInt(self):
        self.optionMap = {'option': 2}
        self.newOpts = {'option': '8'}
        self.expectedResult = {'option': 8}
        mergeDicts(self.optionMap, self.newOpts)
        self.assertDictEqual(self.optionMap, self.expectedResult,
            self.outputMsg("Issue converting string to int"))

    def test_fromFloat(self):
        self.optionMap = {'option': 2}
        self.newOpts = {'option': '8.7'}
        self.expectedResult = {'option': 2}
        caughtIndexError = False
        try:
            mergeDicts(self.optionMap, self.newOpts)
        except ValueError:
            caughtIndexError = True
        self.assertTrue(caughtIndexError, self.outputMsg("Giving float arg to expected int should have raised ValueError"))
        # self.assertDictEqual(self.optionMap, self.expectedResult,
        #     self.outputMsg("Giving float arg to expected int should not alter option map"))



def suite():
    test_suite = []
    test_suite.append(unittest.TestLoader().loadTestsFromTestCase(TestReadConfigFile_emptyLine))
    test_suite.append(unittest.TestLoader().loadTestsFromTestCase(TestReadConfigFile_lineFormatting))
    test_suite.append(unittest.TestLoader().loadTestsFromTestCase(TestMergeDicts_String))
    test_suite.append(unittest.TestLoader().loadTestsFromTestCase(TestMergeDicts_True))
    test_suite.append(unittest.TestLoader().loadTestsFromTestCase(TestMergeDicts_False))
    test_suite.append(unittest.TestLoader().loadTestsFromTestCase(TestMergeDicts_Float))
    test_suite.append(unittest.TestLoader().loadTestsFromTestCase(TestMergeDicts_Int))
    return unittest.TestSuite(test_suite)


if __name__ == "__main__":
    unittest.main()

