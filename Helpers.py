#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date Aug 2018
@file utils.py
@brief Provides common utilities to the RootPlotCombiner project.
"""

from __future__ import print_function, division, absolute_import, unicode_literals

from collections import OrderedDict
from ConfigParser import RawConfigParser
from distutils.util import strtobool
import logging

from logger_cfg import getLogger
logger = getLogger(__name__)


#___________________________________________________________________________
def readConfigFile(configFile):
    """Reads the input config file and build dictionnaries of the sections.

    @param[in] configFile: Opened config file.

    @return: OrderedDict of dict: sectionName -> optionMap.

    The input config file is given opened (easier to write tests).
    The returned dict is ordered to keep track of the order of the plots.
    The returned OrderedDict is a (string: dict) association, keys being
    the section names, values being the option dict (string: string).
    Casting of the option values is done later.
    """
    cfg = RawConfigParser(allow_no_value=False)
    cfg.optionxform = str # To keep case sensitive options.
    cfg.readfp(configFile)

    # Debug info.
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("Parsed content of config file...")
        for section in cfg.sections():
            logger.debug("---> %s <---", section)
            for key, value in cfg.items(section):
                logger.debug("%-15s : %s", key.ljust(15), value)
            logger.debug("-"*40)

    ret = OrderedDict({})
    for section in cfg.sections():
        ret[section] = dict(cfg.items(section))
    del cfg
    return ret


#___________________________________________________________________________
def mergeDicts(defaultOpts, newOpts):
    """Put every value from newOpts to defaultOpts, casting type and
        checking existence.

    @param[in,out] defaultOpts: Complete OrderedDict of possible options,
        with all default values.
    @param[in] newOpts: Options to be merged into defaultOpts.

    If the key read from newOpts is not present in defaultOpts, the
    option is skipped. The value read (string) from newOpts is cast to
    the type of the corresponding option in defaultOpts. For bool
    options, values are cast using distutils.util.strtobool.
    """
    for key, value in newOpts.items():
        key = unicode(key)
        if not key in defaultOpts:
            logger.warning("Option '%s' is not recognized. Skipping...", key)
            continue

        # Get defaultOpts type for this option.
        initialType = type(defaultOpts[key])

        if initialType is bool:
            # Override the behaviour for boolean options.
            # distutils.util.strtobool return 0 or 1 => convert to bool
            defaultOpts[key] = bool(strtobool(value))
        else:
            # Insert cast value in the defaultOpts map.
            defaultOpts[key] = (initialType)(value)

        logger.debug("Option: %s -> read value: %s (new value: %s)",
                     key, value, defaultOpts[key])
        logger.debug("Old type %s should match new type %s cast from type %s",
                     initialType, type(defaultOpts[key]), type(value))
    return


#___________________________________________________________________________
def stringToList(inString, typ=str, sep=','):
    """Split a string and cast its elements to float.

    @param[in] inString: the string to parse.
    @param[in] sep: separator for the split function.

    @return: None if empty string, a list of float otherwise.
    """
    if not inString: return None
    return [typ(token.strip()) for token in inString.split(sep)]


#___________________________________________________________________________
def checkAUTO(inString, typ):
    """Check if a string matches 'auto', if not cast the value to given type.

    @param[in] inString: string to be checked.
    @param[in] typ: type of returned value if not auto.

    @return: pair: bool for 'auto' matched, and value or None for the cast value.
    """
    if "auto" in inString.lower():
        return True, None
    return False, typ(inString.strip())

