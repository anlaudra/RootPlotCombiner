RootPlotCombiner
================

A package to easily superimpose ROOT histogram using config files.

Standard usage: `./RootPlotCombiner.py path/to/configFile.conf`

Detailed usage: see `./RootPlotCombiner.py -h`

Needs ROOT to be setup (`source /path/to/root-source/bin/thisroot.sh`) with python.


General functioning
-------------------

The configuration is handled with a simple txt file.
It has a standard config file format with section headers enclosed in `[...]`,
and an option list following with the format `option = value`.
It is parsed using the `ConfigParser.RawConfigParser` class,
so the following properties are taken into account:

- empty lines, lines containing only spaces or commented lines (#) are ignored.
- lines starting with spaces are not ignored (even if first non space character is #).
- keys are right-stripped, values are left- and right-stripped.
- options and values are case sensitive.
- options lines must contain at least one '=' sign (key only option not allowed).
- values can be empty (ie. lines as `option =` are ok).
- = and # signs are allowed in values (`cutVar = njet==2`).
- some options accept a list of parameters, coma-separated, with or without spaces (stripped).
- some options accept the keyword 'auto' (case insensitive), for the value to be determined
  automatically.
- option names can be multi-words.
- boolean values for options are cast using `distutils.util.strtobool`,
  so `0, 1, true, False`..., are allowed.

Version 2 of this package moved to a content / format way of describing the plots,
thus the section names are less descriptive than in version 1:
they are not specifying the plots config (`1D/2D/profile`, `projected/read`, ...) anymore,
but just specifying if it is the description of a `hist`, or a `plot` (curve), or a `text` zone...

Version 3 introduced the 'auto' keyword, inducing config file interface change on commonly used
options (`ymin`, `ymax`, ...).

The general layout of the config is a `[master]` section setting the formatting of the final plot,
then `[%hist%...]` sections describing what input data (histograms) will be needed for plotting,
and `[%plot%...]` sections describing what should be plotted, how, and where.

Additional text can be drawn to the plot adding `[%text%...]` sections.

A complete list of options can be found below.


Specifications
--------------

The projection or read method for a `hist` is deduced from the option map:
if the name of a histogram is specified, try to retrieve it from the specified input file,
otherwise fall back to projection.
The input file path should always be specified.

In the `[%plot%...]` section, the required inputs are set by the names
of the corresponding `[%hist%...]` sections.
For example, assuming one has two hist sections `[%hist%histogram1]` and `[%hist%histogram2]`
and wants to plot the ratio of the two, the `[%plot%some_name]` will contain options
`histMain = histogram1` and `histDiv = histogram2`.

The localisation of the plots (main pad or ratio pad) is controlled
by the `plotOn` option of `[%plot%...]` section:

- if value is `main` (default), the plot will be put on the main pad.
- if value is `ratio`, the plot will be put on the ratio pad,
  provided the `master` option `ratioPanel` has been switched on.

When projecting histograms, one can either provide `(nbins, xmin, xmax)` or the binning list.
The latter is set using `binningX` and is the first tested method. If the list is blank, histogram
building falls back to the other scheme.
This is valid for both X- and Y-axis.

In case histograms have very close values, it is possible to slightly shifts them
using the option `shiftX = <some value>`.

The overall plot boundaries are automatically computed from all input histograms, but this can be
overriden with use of `xmin`, `xmax`, `ymin`, `ymax` options in the `master` section.

A plot will create a legend entry only if `legendEntry` is not blank.
The legend is always drawn on the main pad.

By default the output files created will be written in the same directory
and with the same name as the config file.
This behaviour can be overridden with master options `outputDir` and `outputName`.
One can use none, both or one of these option, the rest will be deduced from the config file
path/name.
The output format is `eps` and `png` by default, but this can be changed through the `outputFmt`
option.

The `master` section is mandatory. The config file must as well contain at least one hist and one
plot section.

Currently nothing prevents you from superimposing multiple 2D histograms or 1D to 2D
but this is undefined behaviour (not tested).


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


Typical example
---------------

```
[master]
#outputFmt   = pdf, C
xTitle      = m_{#gamma#gamma} [GeV]
yTitle      = Number of events / GeV
#ymin        = 0
#ymax        = 1
#logY        = False
legXmin     = 0.12
legXmax     = 0.88
ratioPanel  = True
ratioTitle  = data/mc
yminRatio   = 0.95
ymaxRatio   = 1.05

[%text%selection]
textLine    = #N_{jets} >= 2
textLine2   = #int L dt = 139 fb^{-1}
textXmin    = 0.12
textYmax    = 0.75

[%hist%mc]
inputFile = /path/to/inputFile1.root
inputHist = diphoton_invmass

[%hist%data]
projType    = 1D
inputFile   = /path/to/inputFile2.root
inputTree   = tree_incl_all
projVar     = m_yy
cutVar      = (n_jet > 1)*weight
nBinsX      = 50
xmin        = 100
xmax        = 150
#normalize   = False

[%plot%mc]
histMain    = mc
drawOpt     = HIST
legendEntry = MC
lineColor   = 632

[%plot%data]
histMain    = data
drawOpt     = E
legendEntry = Data
markerStyle = 23

[%plot%ratio]
plotOn      = ratioPanel
histMain    = projected_hist
histDiv     = read_hist
drawOpt     = E
markerStyle = 23
```


Complete list of options
------------------------

### master

Section header: `[master]`

**General...**

- `batchMode`       : Tells ROOT to run in batch mode as with `root -b` (`default: True`).
- `outputDir`       : Path to output directory (`default: ""`)
- `outputName`      : Basename (no extension) of output file(s) (`default: ""`).
- `outputFmt`       : Extension to save the canvas, as a coma-separated list (`default: eps,png`).
  Adding `root` as output format will save all formatted input histograms in a file (same name as
  other outputs).
- `ratioPanel`      : Switch for adding a ratio pad below the main one (`default: False`).

**Style...**

- `baseStyle`       : Base style (`default: "Modern"`).
- `ATLASStyle`      : Use ATLAS macro for style (`default: False`). Will also plot the ATLAS label.
- `ATLASLabel`      : ATLAS sub-label, ie. Internal/Prelim./... (`default: ""`).
- `ATLASLabX`, `ATLASLabY`  : Left/Top position for the ATLAS label (`default: 0.20, 0.88`).
- `canvSizeX`, `canvSizeY`  : Canvas width/height (default: 800, 600).
- `topMargin`, `rightMargin`, `bottomMargin`, `leftMargin` :
  Canvas margins (`default: 0.10, 0.08, 0.13, 0.13`).
  Should be modified when running 2D plots (extend right margin to allow for Z axis.
- `bothSideAxes`    : Draw ticks on both sides (`default: True`).
- `showGridX`, `showGridY`  : Draw grid along X/Y axis (`default: False, False`).

**Axes titles...**

- `xTitleOff`       : Offset for the x/y-axis as if there was only one pad (`default: 1.2, 1.4`).
  If two pads, the values are automatically scaled according to the size ratio of the two pads.
- `xTitle`          : X-axis title (`default: ""`). Place on ratio pad if present, on main pad otherwise.
- `yTitle`          : Y-axis title for the main pad (`default: ""`).
- `yTitleRatio`     : Y-axis title for the ratio pad (`default: "Ratio"`).

**Axes ranges...**

- `xmin`, `xmax`    : X-axis min/max, can be used to resize histogram (`default: 'auto', 'auto'`).
  If `'auto'` is set, computed from the min/max of all provided histograms.
  Else, the value is cast to float.
- `ymin`, `ymax`    : Y-axis min/max for the main frame (`default: 'auto', 'auto'`).
  If `'auto'` is set, computed from the min/max of all provided histograms to the main panel.
  Else, the value is cast to float.
- `yminRatio`       : Y-axis minimum for the ratio frame (`default: 0.85`).
- `ymaxRatio`       : Y-axis maximum for the ratio frame (`default: 1.15`).
- `logX`, `logY`    : Switch to set log scale on X/Y axis (`default: False, False`)

**Legend...**

- `legendTextSize`  : Legend text size (`default: 0.05`).
- `legXmin`, `legYmax`  : Left/Top position for the legend (`default: 0.12, 0.88`).
  The height bound is automatically computed, the width is fixed to 0.25.


### hist

Section header: `[%hist%some_name]`

- `inputFile`     : Input file path (`default: ""`).
  A TChain is build from the coma-separated values. In case of read, only the first path is taken into account.
- `inputHist`     : Name of the hist to fetch from the input file (`default: ""`). Used by default.
- `inputTree`     : Name of the tree to project from in the input file (`default: ""`). Used if `inputHist` is blank.
- `projType`      : What kind of histogram should receive the data (`default: ""`).
  Only used in case of projection. Allowed values are `1D`, `2D`, `1D profile`.
- `projVar`       : Name of the variable in the input tree to project (`default: ""`).
  Only used in case of projection.
- `cutVar`        : Cut to apply during projection (`default: ""`).
  Only used in case of projection.
- `binningX`      : Comma-separated list of bin boundaries for X axis (`default: ""`).
  Only used in case of projection.
- `nBinsX`        : Number of bins on the x-axis (`default: 100`).
  Only used in case of projection.
- `xmin`, `xmax`  : Boundaries for the x-axis (`default: 0., 1.`).
  Only used in case of projection.
- `binningY`      : Comma-separated list of bin boundaries for Y axis (`default: ""`).
  Only used in case of 2D projection.
- `nBinsY`        : Number of bins on the y-axis (`default: 100`).
  Only used in case of 2D projection.
- `ymin`, `ymax`  : Boundaries for the x-axis (`default: 0., 1.`).
  Only used in case of 2D projection.
- `normalize`     : Normalize histogram to 1 (`default: False`).


### plot

Section header: `[%plot%some_name]`

- `plotOn`        : Pad to plot on (`default: "main"`). Allowed values are `main` and `ratio`.
- `histMain`      : Main histogram forming this plot (`default: ""`).
  Expected values are names of `[%hist%...]` sections (without `%hist%`).
- `histSub`       : Histogram to be subtracted from the main one (`default: ""`). Expected values as above.
- `histDiv`       : Histogram to be divided from the main one (`default: ""`). Expected values as above.
- `shiftX`        : Shift the x-axis of this histogram toward the right (`default: 0.`).
  Useful in case histogram have close values that superimpose.
- `drawOpt`       : Option to be passed to the ROOT `Draw` method (`default: ""`).
- `legendEntry`   : Legend name for this histogram (`default: ""`). If blank, no legend entry will be added.
- `legendType`    : Legend drawing option (`default: "LP"`).
- `lineColor`     : Line color (`default: 1`).
- `lineStyle`     : Line style (`default: 1`).
- `lineWidth`     : Line width (`default: 1`).
- `fillColor`     : Fill color (`default: 0`).
- `fillStyle`     : Fill style (`default: 0`).
- `fillAlpha`     : Fill alpha (`default: 1.`).
- `markerColor`   : Marker color (`default: 1`).
- `markerSize`    : Marker size (`default: 1`).
- `markerStyle`   : Marker style (`default: 1`).


### text

Section header: `[%text%some_name]`

- `plotOn`        : Pad to plot on (`default: "main"`). Allowed values are `main` and `ratio`.
- `textLine`, `textLine2`, `3`, `4`, `5`  : Text to be added to this text pave (`default: ""`).
- `textXmin`, `textYmax`    : Left/Top position for the text pave (`default: 0.12, 0.68`).
- `textColor`     : Text color (`default: 1`).
- `textAlpha`     : Text alpha (`default: 1.`).
- `textSize`      : Text size (`default: 0.035`).
- `textAngle`     : Text angle (`default: 0. # degrees`).

