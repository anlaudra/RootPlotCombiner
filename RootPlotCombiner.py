#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date Nov. 2018, last update November 2020
@file RootPlotCombiner.py
@brief Steering script for the RootPlotCombiner module.
"""

from __future__ import print_function, division, absolute_import, unicode_literals

import sys
import argparse
import logging

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True #pylint: disable=wrong-import-position
# Or command line args are passed to ROOT instead of program.

from MasterCombiner import MasterCombiner
from logger_cfg import getLogger, setup_logging


#_______________________________________________________________________________
def parse_args(argv):
    # pylint: disable=missing-docstring
    # pylint: disable=bad-continuation
    parser = argparse.ArgumentParser()
    parser.add_argument('configFilePathList',
        nargs="+", metavar='path/to/configFile.conf',
        help="Path to the config files to build the plots from.")

    verbosity = parser.add_mutually_exclusive_group()
    verbosity.add_argument('--verbose', '-v', action='count', default=0,
        help="Verbosity level. -v prints maps, -vv prints debug.")
    verbosity.add_argument('--quiet', '-q', action='count', default=0,
        help="Reduce verbosity (default is INFO).")

    return vars(parser.parse_args(argv))


#_______________________________________________________________________________
def main(configFilePathList, verbose=0, quiet=0):
    """Entry point of the RootPlotCombiner package.

    @param[in] configFilePathList: list of paths to config file.
    @param[in] verbose: increase verbosity level (default 0 -> INFO).
    @param[in] quiet: decrease verbosity level (default 0 -> INFO).
    """
    setup_logging()
    root_logger = getLogger()
    if   verbose == 1: root_logger.setLevel(logging.VERBOSE)
    elif verbose >= 2: root_logger.setLevel(logging.DEBUG)
    elif quiet   >= 1: root_logger.setLevel(logging.WARNING)

    for configFilePath in configFilePathList:
        combiner = MasterCombiner(configFilePath)
        combiner.combinePlots()
        del combiner
        print()
    return


#_______________________________________________________________________________
if __name__ == "__main__":
    main(**parse_args(sys.argv[1:]))

