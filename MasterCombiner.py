#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date Jan. 2017 - Nov. 2018
@file MasterCombiner.py
@brief Reads a config file to superimpose plots using ROOT.
"""

from __future__ import print_function, division, absolute_import, unicode_literals

import sys
import os.path
import logging
from collections import OrderedDict
from ROOT import gROOT, gStyle, gPad, TColor, TCanvas, TPad, TFile, TLegend, TLatex, kWhite

from Helpers import readConfigFile, mergeDicts, stringToList, checkAUTO
from HistBuilder import HistBuilder
from PlotFormatter import PlotFormatter
from TextFormatter import TextFormatter
from logger_cfg import getLogger
logger = getLogger(__name__)


#_______________________________________________________________________________
def defaultMasterOptionMap():
    """Create the default option map."""
    masterOptions = OrderedDict({})
    masterOptions["batchMode"]      = True
    masterOptions["outputDir"]      = ""
    masterOptions["outputName"]     = ""
    masterOptions["outputFmt"]      = "eps, png"
    masterOptions["saveAsROOT"]     = False
    masterOptions["ratioPanel"]     = False
        # Style...
    masterOptions["baseStyle"]      = "Modern"
    masterOptions["ATLASStyle"]     = False
    masterOptions["ATLASLabel"]     = ""
    masterOptions["ATLASLabX"]      = 0.20
    masterOptions["ATLASLabY"]      = 0.88
    masterOptions["canvSizeX"]      = 800
    masterOptions["canvSizeY"]      = 600
    masterOptions["topMargin"]      = 0.10 # atlas 0.05
    masterOptions["rightMargin"]    = 0.08 # atlas 0.05
    masterOptions["bottomMargin"]   = 0.13 # atlas 0.16
    masterOptions["leftMargin"]     = 0.13 # atlas 0.16
    masterOptions["bothSideAxes"]   = True
    masterOptions["showGridX"]      = False
    masterOptions["showGridY"]      = False
        # Axes titles...
    masterOptions["xTitleOff"]      = 1.2 # atlas 1.4
    masterOptions["yTitleOff"]      = 1.4 # atlas 1.4
    masterOptions["xTitle"]         = ""
    masterOptions["yTitle"]         = ""
    masterOptions["yTitleRatio"]    = "Ratio"
        # Axes ranges...
    masterOptions["xmin"]           = "auto"
    masterOptions["xmax"]           = "auto"
    masterOptions["ymin"]           = "auto"
    masterOptions["ymax"]           = "auto"
    masterOptions["yminRatio"]      = 0.85
    masterOptions["ymaxRatio"]      = 1.15
    masterOptions["logX"]           = False
    masterOptions["logY"]           = False
        # Legend...
    masterOptions["legendTextSize"] = 0.045
    masterOptions["legXmin"]        = 0.12
    masterOptions["legYmax"]        = 0.88 # assuming the legend is on the top, the ylow bound is auto computed
    return masterOptions



#_______________________________________________________________________________
class MasterCombiner(object):
    """Master class handling the plot superimposition.

    WARNING: Make sure to properly type the variables in the template
    maps (used to cast the string of the option reader).
    See https://root.cern.ch/doc/master/RtypesCore_8h_source.html
    Configuration: `configFilePath`, `masterOptions`.
    Main callable: `combinePlots()`.
    Deliverable: `canv`.
    1.  Read and parse the provided config file.
        Split the recognized sections to hist, plot and text config dicts.
        Fill master map.
    2.  Setup the global ROOT style. Need info from master config.
    3.  Call the hist builders. Internal processing needs info from hist configs.
    4.  Call the plot and text formatters. Need info from the previous builders.
        Internal processing needs info from plot and text configs.
    5.  Prepare the canvas hosting everything.
    6.  Prepare the pads, setting the split ratio (hard-coded).
        Both are built, even in case of the ratio pad size being 0.
        Need info from the plot formatters.
    7.  Prepare the frames for each pad (axes ranges...).
        Resize the labels and titles according to the split ratio.
        Draw the axes titles.
        Need info from the plot formatters.
    8.  Draw the plots on the pad corresponding to their config (`plotOn`).
    9.  Build the legend. Only formatters with non blank `legendEntry` option
        are used.
    10. Output final plot, and ROOT file if required.
    """

    #___________________________________________________________________________
    def __init__(self, configFilePath):
        self._configFilePath = configFilePath
        self._masterOptions = defaultMasterOptionMap()
        self._histConfigs = OrderedDict({})
        self._plotConfigs = OrderedDict({}) # OrderedDict of dict with {sectionName: optionDict}, ordered so that plots are correctly ordered.
        self._textConfigs = OrderedDict({})
        self._histBuilders = {} # map hist name -> builder
        self._plotFormatters = [] # list of hist formatters to be plotted
        self._textFormatters = [] # list of text formatters to be plotted
        self._canv = None
        self._padMain = None
        self._padRatio = None
        self._frameMain = None
        self._frameRatio = None
        self._ratioSplitSize = 0 # default value for splitting, updated later if needed
        self._outPathNoExt = ""

        # Following should provide color codes starting at 1179.
        color_signal    = TColor.GetColor("#67B2FF") # Closest: 856 (kAzure-4)
        color_qqZZ      = TColor.GetColor("#E20D0A") # Closest: 633 (kRed+1)
        color_redBkg    = TColor.GetColor("#793FD2") # Closest: 885 (kViolet+5) or -5, sometimes 417 (kGreen+1)
        color_ttV       = TColor.GetColor("#EDE810") # Closest: 400 (kYellow)
        color_VBF       = TColor.GetColor("#99E148") # Closest: 825 (kSpring+5)
        color_VH        = TColor.GetColor("#EB9B35") # Closest: 801 (kOrange+1)
        color_ttH       = TColor.GetColor("#212ADE") # Closest: 597 (kBlue-3)
        color_tH        = TColor.GetColor("#DF71D1") # Closest: 906 (kPink+6)
        color_qqZZjj    = TColor.GetColor("#B5236F") # Closest: 895 (kPink-5)
        # color_ttbar     = 797 (kOrange-3)
        # color_WZ        = 920 (kGrey)


    #___________________________________________________________________________
    def _printOptionMap(self):
        """Print the master option map."""
        if not logger.isEnabledFor(logging.VERBOSE):
            return
        logger.verbose("Map content for master:")
        for (key, value) in self._masterOptions.items():
            logger.verbose("%-15s : %s", key, value)
        return


    #___________________________________________________________________________
    def _loadConfigFile(self):
        """Reads the input config file and build dictionaries of the sections.

        The file to be open is defined by self.configFilePath.
        The plot config sections are appended to the self._plotConfigs
        list, while the master section is directly merged into the
        masterOptions map.
        """
        # Open the config file.
        try:
            with open(self._configFilePath, 'r') as inFile:
                sections = readConfigFile(inFile)
        except IOError:
            logger.error("Unable to open config file: %s", self._configFilePath)
            logger.error("Will now exit.")
            sys.exit(1)
        logger.info("Successfully opened config file: %s", self._configFilePath)

        # We must have at least the 'master' section...
        if 'master' not in sections:
            logger.error("No 'master' section found in config file %s",
                         self._configFilePath)
            logger.error("Found headers: %s", sections)
            logger.error("Will now exit.")
            sys.exit(1)

        # Add the plot option sections to the config dictionnary.
        for section in sections:
            if section == 'master':
                continue
            elif "%hist%" in section:
                logger.debug("Found hist section: %s", section)
                self._histConfigs[section] = sections[section]
            elif "%plot%" in section:
                logger.debug("Found plot section: %s", section)
                self._plotConfigs[section] = sections[section]
            elif "%text%" in section:
                logger.debug("Found text section: %s", section)
                self._textConfigs[section] = sections[section]
            else:
                logger.warning("Section '%s' is not recognized as 'hist', "
                               "'plot', or 'text'. Skipping...", section)

        # ... and one hist config section read.
        if not self._histConfigs:
            logger.error("No hist config section found.")
            logger.error("Will now exit.")
            sys.exit(1)

        # ... and one plot config section read.
        if not self._plotConfigs:
            logger.error("No plot config section found.")
            logger.error("Will now exit.")
            sys.exit(1)

        # Update the master options with what was found in the 'master' section.
        mergeDicts(self._masterOptions, sections['master'])
        return


    #___________________________________________________________________________
    def _buildOutputName(self):
        """Build the output name (without file extension).

        The output base name is built from the master options if provided, else
        assume the output files should be in the same dir as the config file and
        with the same name (just changing the extension).
        """
        # First build the output dir name.
        logger.debug("Building output dir and output file names...")
        outDirName = ""
        if self._masterOptions['outputDir'] == "":
            logger.debug("masterOptions['outputDir'] is empty, "
                         "taking input config file path...")
            outDirName = os.path.dirname(self._configFilePath)
        else:
            logger.debug("Using masterOptions['outputDir']...")
            outDirName = self._masterOptions['outputDir']
        logger.debug("outDir name before norm: '%s'", outDirName)
        outDirName = os.path.normpath(outDirName)

        # Check that the output dir exists.
        if not os.path.isdir(outDirName):
            logger.error("Resulting outDirName name does not exist: '%s'",
                         outDirName)
            logger.error("Please first run 'mkdir %s'", outDirName)
            logger.error("Will now exit.")
            sys.exit(1)
        logger.debug("Checked resulting outDirName name exists: '%s'",
                     outDirName)

        outFileName = ""
        # Then build the output file name without extension.
        if self._masterOptions['outputName'] == "":
            logger.debug("masterOptions['outputName'] is empty, "
                         "taking input config file name...")
            outFileName = os.path.basename(self._configFilePath)
            # Remove trailing .conf.
            outFileName = os.path.splitext(outFileName)[0]
        else:
            logger.debug("Using masterOptions['outputName']...")
            outFileName = self._masterOptions['outputName']

        self._outPathNoExt = os.path.join(outDirName, outFileName)
        logger.debug("Output path before norm: %s", self._outPathNoExt)
        self._outPathNoExt = os.path.normpath(self._outPathNoExt)
        logger.info("Will write files: '%s.*'", self._outPathNoExt)
        return


    #___________________________________________________________________________
    def _applyStyle(self):
        """Apply the canvas style defined by the options."""

        logger.debug("Applying standard style...")
        gROOT.SetBatch(            self._masterOptions["batchMode"])
        gROOT.SetStyle(            self._masterOptions["baseStyle"])
        gStyle.SetTitleOffset(     self._masterOptions["xTitleOff"], "x")
        gStyle.SetTitleOffset(     self._masterOptions["yTitleOff"], "y")
        gStyle.SetPadTopMargin(    self._masterOptions["topMargin"])
        gStyle.SetPadRightMargin(  self._masterOptions["rightMargin"])
        gStyle.SetPadBottomMargin( self._masterOptions["bottomMargin"])
        gStyle.SetPadLeftMargin(   self._masterOptions["leftMargin"])
        gStyle.SetLegendBorderSize(0)
        gStyle.SetLegendFillColor(kWhite)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
        gStyle.SetOptFit(0)
        # # Added from ATLAS style, but used as default
        # gStyle.SetFrameBorderMode(0)
        # gStyle.SetFrameFillColor(0)
        # gStyle.SetCanvasBorderMode(0)
        # gStyle.SetCanvasColor(0)
        # gStyle.SetPadBorderMode(0)
        # gStyle.SetPadColor(0)
        # gStyle.SetStatColor(0)
        gStyle.SetTitleSize(0.045, "xyz")
        gStyle.SetLabelSize(0.045, "xyz")
        gStyle.SetTitleFont(42, "xyz") # ATLAS 42 (Helvetica), default 62
        gStyle.SetLabelFont(42, "xyz") # ATLAS 42 (Helvetica), default 62
        logger.debug("Applied standard style.")
        return


    #___________________________________________________________________________
    def _enforceATLASStyle(self):
        """Define the official ATLAS style, can be called to enforce
            ATLAS style.
        """
        logger.debug("Applying ATLAS style...")
        gStyle.SetPaperSize(20, 26)
        gStyle.SetFrameBorderMode(0)
        gStyle.SetFrameFillColor(0)
        gStyle.SetCanvasBorderMode(0)
        gStyle.SetCanvasColor(0)
        gStyle.SetPadBorderMode(0)
        gStyle.SetPadColor(0)
        gStyle.SetStatColor(0)
        gStyle.SetPadTopMargin(0.05)
        gStyle.SetPadRightMargin(0.05)
        gStyle.SetPadBottomMargin(0.16)
        gStyle.SetPadLeftMargin(0.16)
        # set title offsets (for axis label)
        gStyle.SetTitleXOffset(1.4)
        gStyle.SetTitleYOffset(1.4)
        gStyle.SetTitleSize(0.05, "xyz")
        gStyle.SetLabelSize(0.05, "xyz")
        gStyle.SetTitleFont(42, "xyz") # ATLAS 42 (Helvetica), default 62
        gStyle.SetLabelFont(42, "xyz") # ATLAS 42 (Helvetica), default 62
        # use bold lines and markers
        gStyle.SetMarkerStyle(20)
        gStyle.SetMarkerSize(1.2)
        gStyle.SetHistLineWidth(2)
        gStyle.SetLineStyleString(2, "[12 12]") # postscript dashes
        # get rid of error bar caps
        gStyle.SetEndErrorSize(0)
        # do not display any of the standard histogram decorations
        gStyle.SetOptTitle(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptFit(0)
        # put tick marks on top and RHS of plots
        gStyle.SetPadTickX(1)
        gStyle.SetPadTickY(1)
        logger.info("Applied ATLAS style.")
        return


    #___________________________________________________________________________
    def _drawATLASLabel(self):
        """Draw an ATLAS label, with additional word provided by the
            ATLASLabel option.
        """
        logger.debug("Putting ATLAS label with extra word '%s' "
                     "at coord. %.3f %.3f",
                     self._masterOptions["ATLASLabel"],
                     self._masterOptions["ATLASLabX"],
                     self._masterOptions["ATLASLabY"])
        atlasLatex = TLatex()
        atlasLatex.SetNDC()
        atlasLatex.SetTextFont(72)
        atlasLatex.SetTextColor(1)
        labelWidth = 0.120 * (696*gPad.GetWh()) / (472*gPad.GetWw()) # Original factor: 0.115.
        atlasLatex.DrawLatex(self._masterOptions["ATLASLabX"],
                             self._masterOptions["ATLASLabY"],
                             "ATLAS")
        auxLatex = TLatex()
        auxLatex.SetNDC()
        auxLatex.SetTextFont(42)
        auxLatex.SetTextColor(1)
        auxLatex.DrawLatex(self._masterOptions["ATLASLabX"] + labelWidth,
                           self._masterOptions["ATLASLabY"],
                           self._masterOptions["ATLASLabel"])
        return


    #___________________________________________________________________________
    def _callBuilders(self):
        """Call the builders."""

        logger.debug("Calling the builders...")
        for name, newOptions in self._histConfigs.items():
            if not "%hist%" in name:
                logger.warning("Section '%s' in _histConfigs is not a hist, "
                               "skipping.", name)
                continue
            logger.info("Building hist for section: %s", name)
            builder = HistBuilder(name, newOptions)
            builder.buildHistogram()
            self._histBuilders[builder.hist.GetName()] = builder
        logger.debug("All builders called.")
        return


    #___________________________________________________________________________
    def _callFormatters(self):
        """Call the Plot and Text formatters."""

        logger.debug("Calling the formatters...")
        for name, newOptions in self._plotConfigs.items():
            if not "%plot%" in name:
                logger.warning("Section '%s' in _plotConfigs is not a plot, "
                               "skipping.", name)
                continue
            logger.info("Formatting plot for section: %s", name)
            formatter = PlotFormatter(name, newOptions, self._histBuilders)
            formatter.formatPlot()
            self._plotFormatters.append(formatter)
        logger.debug("All plot formatters called.")

        for name, newOptions in self._textConfigs.items():
            if not "%text%" in name:
                logger.warning("Section '%s' in _textConfigs is not a text, "
                               "skipping.", name)
                continue
            logger.info("Formatting text for section: %s", name)
            formatter = TextFormatter(name, newOptions)
            formatter.formatText()
            self._textFormatters.append(formatter)
        logger.debug("All text formatters called.")
        return


    #___________________________________________________________________________
    def _setupPads(self):
        """Build the main and ratio pads.

        Setup log and grid for these pads, then draw to make them available.
        """
        logger.debug("Setting up pads...")
        if self._masterOptions["ratioPanel"]:
            self._ratioSplitSize = 0.3
        self._padMain = TPad(
            "padMain", "padMain", 0, self._ratioSplitSize, 1, 1)

        self._padMain.SetLogx(self._masterOptions["logX"])
        self._padMain.SetLogy(self._masterOptions["logY"])
        self._padMain.SetGrid(self._masterOptions["showGridX"],
                              self._masterOptions["showGridY"])

        if self._masterOptions["ratioPanel"]:
            logger.debug("This plot has a ratio panel.")
            self._padRatio = TPad(
                "padRatio", "padRatio", 0, 0, 1, self._ratioSplitSize)
            self._padMain.SetBottomMargin(0)
            self._padRatio.SetTopMargin(0)
            self._padRatio.SetBottomMargin(0.4)
            # For the ratio pad, never set logY, always set Y grid.
            self._padRatio.SetLogx(self._masterOptions["logX"])
            self._padRatio.SetGrid(self._masterOptions["showGridX"], True)
            self._padRatio.Draw()
        logger.debug("Main / ratio panel split size was: %s",
                     self._ratioSplitSize)
        self._padMain.Draw()

        logger.debug("Pads set up.")
        return


    #___________________________________________________________________________
    def _setupFrames(self):
        """Setup the frames corresponding to each pads. Set the axes titles."""

        logger.debug("Setting up the frames...")
        # Get the extremal axis ranges.
        autoXmin, xmin = checkAUTO(self._masterOptions["xmin"], float)
        autoXmax, xmax = checkAUTO(self._masterOptions["xmax"], float)
        if autoXmin:
            logger.debug("xmin found to be auto, computing xmin.")
            xmin = min([formatter.hist.GetXaxis().GetXmin()
                        for formatter in self._plotFormatters])
        if autoXmax:
            logger.debug("xmax found to be auto, computing xmax.")
            xmax = max([formatter.hist.GetXaxis().GetXmax()
                        for formatter in self._plotFormatters])
        logger.debug("Final x-axis min %f and max %f", xmin, xmax)

        # If the auto range option is set, find the ymin and ymax from hists
        # (only for main panel).
        # Leave a bit of space on top and bottom.
        # For the high bound, if positive value scale by 1.05,
        # if negative scale by 0.90. Do the opposite for low bound.
        autoYmin, ymin = checkAUTO(self._masterOptions["ymin"], float)
        autoYmax, ymax = checkAUTO(self._masterOptions["ymax"], float)
        if autoYmin:
            logger.debug("ymin found to be auto, computing ymin.")
            ymin = min([formatter.hist.GetMinimum()
                        for formatter in self._plotFormatters
                        if "main" in formatter.plotOptions["plotOn"].lower()
                       ])
            ymin = 0.90 * ymin if ymin > 0 else 1.05 * ymin
        if autoYmax:
            logger.debug("ymax found to be auto, computing ymax.")
            ymax = max([formatter.hist.GetMaximum()
                        for formatter in self._plotFormatters
                        if "main" in formatter.plotOptions["plotOn"].lower()
                       ])
            ymax = 1.05 * ymax if ymax > 0 else 0.90 * ymax
        logger.debug("Final y-axis min %f and max %f", xmin, ymax)

        # Create the frames.
        if self._masterOptions["ratioPanel"]:
            logger.debug("Creating ratio frame with xmin/xmax = %s/%s, "
                         "ymin/ymax = %s/%s",
                         xmin, self._masterOptions["yminRatio"],
                         xmax, self._masterOptions["ymaxRatio"])
            self._padRatio.cd()
            self._frameRatio = self._padRatio.DrawFrame(
                xmin, self._masterOptions["yminRatio"],
                xmax, self._masterOptions["ymaxRatio"])
            self._frameRatio.SetNameTitle("frameRatio", "frameRatio")
            self._frameRatio.Sumw2()

        logger.debug("Creating main frame with xmin/xmax = %s/%s, "
                     "ymin/ymax = %s/%s", xmin, ymin, xmax, ymax)
        self._padMain.cd()
        self._frameMain = self._padMain.DrawFrame(xmin, ymin, xmax, ymax)
        self._frameMain.SetNameTitle("frameMain", "frameMain")
        self._frameMain.Sumw2()

        # Put axis labels.
        if self._masterOptions["ratioPanel"]:
            self._frameRatio.SetXTitle(self._masterOptions["xTitle"])
            self._frameRatio.SetYTitle(self._masterOptions["yTitleRatio"])
        else:
            self._frameMain.SetXTitle(self._masterOptions["xTitle"])
        self._frameMain.SetYTitle(self._masterOptions["yTitle"])

        logger.debug("Frames set up.")
        return


    #___________________________________________________________________________
    def _adaptPadsTextSize(self):
        """Rescale axes label and titles due to splitting."""
        #pylint: disable=line-too-long

        if not self._masterOptions["ratioPanel"]: return
        logger.debug("Adapting text sizes...")

        scaleFactorMain =  1. / (1. - self._ratioSplitSize)
        self._frameMain.GetXaxis().SetTitleSize(scaleFactorMain * self._frameMain.GetXaxis().GetTitleSize())
        self._frameMain.GetYaxis().SetTitleSize(scaleFactorMain * self._frameMain.GetYaxis().GetTitleSize())
        self._frameMain.GetXaxis().SetLabelSize(scaleFactorMain * self._frameMain.GetXaxis().GetLabelSize())
        self._frameMain.GetYaxis().SetLabelSize(scaleFactorMain * self._frameMain.GetYaxis().GetLabelSize())
        self._frameMain.GetXaxis().SetTitleOffset(1./scaleFactorMain * self._frameMain.GetXaxis().GetTitleOffset())
        self._frameMain.GetYaxis().SetTitleOffset(1./scaleFactorMain * self._frameMain.GetYaxis().GetTitleOffset())

        scaleFactorRatio = 1. / (1. * self._ratioSplitSize) if self._ratioSplitSize != 0 else 1.
        self._frameRatio.GetXaxis().SetTitleSize(scaleFactorRatio * self._frameRatio.GetXaxis().GetTitleSize())
        self._frameRatio.GetXaxis().SetLabelSize(scaleFactorRatio * self._frameRatio.GetXaxis().GetLabelSize())
        self._frameRatio.GetYaxis().SetTitleSize(scaleFactorRatio * self._frameRatio.GetYaxis().GetTitleSize())
        self._frameRatio.GetYaxis().SetLabelSize(scaleFactorRatio * self._frameRatio.GetYaxis().GetLabelSize() * 0.8)
        self._frameRatio.GetYaxis().SetTitleOffset(1./scaleFactorRatio * self._frameRatio.GetYaxis().GetTitleOffset())
        self._frameRatio.GetYaxis().SetNdivisions(305) # 05 main, 3 sub divisions

        logger.debug("Adapting text sizes: done.")
        return


    #___________________________________________________________________________
    def _buildLegend(self, histFormatters, legXmin, legYmax):
        """Build a legend for the given list of formatter.

        The formatter list is specified (might not be the full list)
        to leave the possibility to split the legend in two.
        """
        logger.debug("Building the legend...")

        # We won't print a legend line if the legend name has not been provided.
        # Count how many entries we will have to plot (to set legend box).
        nLegendLines = 0
        for formatter in histFormatters:
            if not formatter.plotOptions["legendEntry"]:
                logger.debug("Formatter %s has no 'legendEntry",
                             formatter.nameNoOption)
                continue
            logger.debug("Formatter %s has 'legendEntry: '%s'",
                         formatter.nameNoOption,
                         formatter.plotOptions["legendEntry"])
            nLegendLines += 1

        legYmin = legYmax - nLegendLines * self._masterOptions["legendTextSize"]

        logger.debug("Adding %d entries to the legend from %d formatters read.",
                     nLegendLines, len(histFormatters))

        theLegend = TLegend(legXmin, legYmin, legXmin + 0.25, legYmax)
        logger.info("Legend corners: %.3f %.3f %.3f %.3f",
                    theLegend.GetX1(), theLegend.GetY1(),
                    theLegend.GetX2(), theLegend.GetY2())
        theLegend.SetFillColorAlpha(kWhite, 0.)
        theLegend.SetLineColor(kWhite)
        theLegend.SetBorderSize(0)
        theLegend.SetTextSize(self._masterOptions["legendTextSize"])

        # Add the legend entry for needed histograms.
        for formatter in histFormatters:
            logger.debug("Adding legend entry: %s (%s) for object: %s",
                         formatter.plotOptions["legendEntry"],
                         formatter.plotOptions["legendType"],
                         formatter.hist.GetName())
            # If no legend entry provided, do not add it.
            if not formatter.plotOptions["legendEntry"]:
                logger.debug("This legendEntry is empty, skipping.")
                continue
            theLegend.AddEntry(formatter.hist,
                               formatter.plotOptions["legendEntry"],
                               formatter.plotOptions["legendType"])

        return theLegend


    #___________________________________________________________________________
    def _doSaveAsROOT(self):
        """Save the plots in a ROOT file."""
        outFile = TFile(self._outPathNoExt + ".root", "RECREATE")
        outFile.cd()
        for formatter in self._plotFormatters:
            formatter.hist.Write()
        logger.info("Written ROOT file: %s", outFile.GetName())

        outFile.Close()
        del outFile
        return


    #___________________________________________________________________________
    def combinePlots(self):
        """Main callable of the class: reads the histograms built from
            the HistBuilder instances, and plot them together.
        """
        logger.info("Starting...")
        self._loadConfigFile()
        self._printOptionMap()

        self._buildOutputName()

        # Apply style + enforce ATLAS style if asked.
        self._applyStyle()
        if self._masterOptions["ATLASStyle"]:
            self._enforceATLASStyle()

        # Build the histograms from the previously filled sections.
        self._callBuilders()

        # Format the plots needed.
        self._callFormatters()

        logger.info("Creating the canvas...")
        self._canv = TCanvas("canv", "canv", \
            self._masterOptions["canvSizeX"], self._masterOptions["canvSizeY"])
        self._canv.cd()

        # Prepare ratio pad if needed and setup the frames on the canvas.
        self._setupPads()
        self._setupFrames()
        self._adaptPadsTextSize()

        # Plot the histograms.
        for formatter in self._plotFormatters:
            logger.info("Plotting hist %s on frame %s",
                        formatter.nameNoOption, formatter.plotOptions["plotOn"])
            if "main" in formatter.plotOptions["plotOn"].lower():
                self._padMain.cd()
            elif self._masterOptions["ratioPanel"] and "ratio" in formatter.plotOptions["plotOn"].lower():
                self._padRatio.cd()
            else:
                logger.debug("The requested plotting pad is not available, "
                             "skipping.")
                continue
            formatter.hist.Draw("same " + formatter.plotOptions["drawOpt"])

        for formatter in self._textFormatters:
            logger.info("Plotting text %s on frame %s",
                        formatter.nameNoOption, formatter.textOptions["plotOn"])
            if "main" in formatter.textOptions["plotOn"].lower():
                self._padMain.cd()
            elif self._masterOptions["ratioPanel"] and "ratio" in formatter.textOptions["plotOn"].lower():
                self._padRatio.cd()
            else:
                logger.debug("The requested plotting pad is not available, "
                             "skipping.")
                continue
            formatter.textPave.Draw("same")

        # Redraw the axes.
        logger.debug("Redrawing the axes...")
        if self._masterOptions["ratioPanel"]:
            self._padRatio.cd()
            self._frameRatio.Draw("same axis")
            if self._masterOptions["bothSideAxes"]:
                self._frameRatio.Draw("same axis X+ Y+")
        self._padMain.cd()
        self._frameMain.Draw("same axis")
        if self._masterOptions["bothSideAxes"]:
            self._frameMain.Draw("same axis X+ Y+")

        # Build the legend.
        self._padMain.cd()
        legend = self._buildLegend(self._plotFormatters, \
            self._masterOptions["legXmin"], self._masterOptions["legYmax"])
        legend.Draw()

        # Print the ATLAS label if required by ATLASStyle
        if self._masterOptions["ATLASStyle"] or self._masterOptions["ATLASLabel"] != "":
            self._drawATLASLabel()

        # Save output files.
        extensions = stringToList(self._masterOptions["outputFmt"])
        extensions = OrderedDict(zip(extensions, [None]*len(extensions)))
        # Cheat to have an ordered set: I remember having issues if png was
        # done before eps. As OrderedSet does not exist, use OrderedDict.
        # We do not care about they values, just the keys (set to None).
        if "root" in extensions:
            extensions.pop("root")
            self._doSaveAsROOT()
        for ext in extensions:
            self._canv.SaveAs(self._outPathNoExt + "." + ext)

        self._canv.Close()
        logger.info("Finishing...")
        return



#_______________________________________________________________________________
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Please provide the main config file as only argument.")
        sys.exit(1)

    combiner = MasterCombiner(sys.argv[1])
    combiner.combinePlots()

