#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date October 2020
@file logger_cfg.py
@brief Configuration for the framework's logging system.
"""

from __future__ import print_function, division, absolute_import, unicode_literals

import os.path
import logging
import logging.config
from yaml import safe_load

###############################################################################
# Add other logging levels.
# See: https://stackoverflow.com/questions/2183233/how-to-add-a-custom-loglevel-to-pythons-logging-facility

VERBOSE = 15  # Between INFO (20) and DEBUG (10).

logging.VERBOSE = VERBOSE
logging.addLevelName(VERBOSE, "VERBOSE")

def verbose(self, message, *args, **kwargs):
    if self.isEnabledFor(VERBOSE):
        # Yes, logger takes its '*args' as 'args'.
        self._log(VERBOSE, message, args, **kwargs)

logging.Logger.verbose = verbose


###############################################################################
# Setup the logger.

DEFAULT_CFG_FILE = os.path.join(os.path.dirname(__file__), "logger_cfg.yaml")

def setup_logging(cfg_file=DEFAULT_CFG_FILE):
    """Configure logging from a YAML file."""
    if cfg_file is not None and os.path.exists(cfg_file):
        with open(cfg_file) as infile:
            config = safe_load(infile)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=logging.INFO)


def getLogger(name=None):
    """Returns the logger for given name.

    @param[in] name: string or None
        Name of the logger to be retrieved (typically: __name__)
        If None, retrieves the root logger of the package (RootPlotCombiner).

    This is just a wrapper around logging.getLogger(...): it ensures
    all logger are children from a root logger name as the package.
    """
    if name is None:
        return logging.getLogger("RootPlotCombiner")
    return logging.getLogger("RootPlotCombiner." + name)


