#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date Nov. 2018
@file TextFormatter.py
@brief Definition of the TextFormatter class, and of its options.
"""

from __future__ import print_function, division, absolute_import, unicode_literals

import sys
import logging
from collections import OrderedDict
from ROOT import TPaveText, kWhite

from Helpers import mergeDicts
from logger_cfg import getLogger
logger = getLogger(__name__)


#_______________________________________________________________________________
def defaultPlotOptionMap():
    """Create the default option map."""
    textOptions = OrderedDict({})
    textOptions["plotOn"]       = "main"
    textOptions["textLine"]     = ""
    textOptions["textLine2"]    = ""
    textOptions["textLine3"]    = ""
    textOptions["textLine4"]    = ""
    textOptions["textLine5"]    = ""
    textOptions["textXmin"]     = 0.12
    textOptions["textYmax"]     = 0.68
    textOptions["textColor"]    = 1
    textOptions["textAlpha"]    = 1.
    textOptions["textSize"]     = 0.035
    textOptions["textAngle"]    = 0. # degrees
    return textOptions



#_______________________________________________________________________________
class TextFormatter(object):
    """Given a block of options, fill the option map in and setup the
        text pave.

    WARNING: Make sure to properly type the variables in the template
    maps (used to cast the string of the option reader).
    See https://root.cern.ch/doc/master/RtypesCore_8h_source.html
    Configuration: `doDebug`, `name`, `textOptions`.
    Main callable: `setupText()`.
    Deliverable: `textPave`.
    1.  Check that the provided section is a text formatting.
    2.  Setup the correct name for the formatter.
    3.  Prepare the TPaveText (size according to number of non blank
        lines), and position.
    4.  Setup the text.
    """

    def __init__(self, name, newOptions):
        """
        @param[in] name: section header.
        @param[in] newOptions: (unordered) dict of options
            that have to be changed.
        """
        self.doDebug = False
        self._name = name
        self._newOptions = newOptions
        self._textOptions = defaultPlotOptionMap()
        self._textPave = None

    @property
    def nameNoOption(self):
        """Actual name of the instance: section name without leading options."""
        return self._name.replace("%text%", "")

    @property
    def textOptions(self):
        """Map of options provided (not updated before formatText is called)."""
        return self._textOptions

    @property
    def textPave(self):
        """Deliverable: the formatted TPaveText (after formatText is called)."""
        return self._textPave


    #___________________________________________________________________________
    def _printOptionMap(self):
        """Print the map used for this histogram."""
        if not logger.isEnabledFor(logging.VERBOSE):
            return
        logger.verbose("Map content for text: %s (%s)",
                       self.nameNoOption, self._name)
        for (key, value) in self._textOptions.items():
            logger.verbose("%-15s : %s", key, value)
        return


    #___________________________________________________________________________
    def _setupText(self):
        """Prepare the TPaveText."""

        logger.debug("Setting up text...")
        nTextLines = 0
        for line in ["textLine", "textLine2", "textLine3", "textLine4", "textLine5"]:
            logger.debug("%s: '%s'", line, self._textOptions[line])
            if self._textOptions[line] == "":
                logger.debug("%s is empty, skipping.", line)
                continue
            nTextLines += 1

        if nTextLines == 0:
            logger.warning("Only empty line found for text pave %s.",
                           self.nameNoOption)
        logger.debug("Adding %d lines to the text pave.", nTextLines)

        textYmin = self._textOptions["textYmax"] - nTextLines * self._textOptions["textSize"]

        self._textPave = TPaveText(
            self._textOptions["textXmin"], textYmin,
            self._textOptions["textXmin"] + 0.25, self._textOptions["textYmax"],
            "NDC")
        logger.info("Text pave corners: %.3f %.3f %.3f %.3f",
                    self._textPave.GetX1(), self._textPave.GetY1(),
                    self._textPave.GetX2(), self._textPave.GetY2())
        self._textPave.SetFillColorAlpha(kWhite, 0.)
        self._textPave.SetLineColor(kWhite)
        self._textPave.SetBorderSize(0)

        logger.debug("Set up text.")
        return


    #___________________________________________________________________________
    def formatText(self):
        """Main callable."""

        logger.debug("Formatting text %s", self._name)
        if not "%text%" in self._name:
            logger.error("Trying to call PlotFormatter without %text% in "
                         "section name.")
            logger.error("Will now exit.")
            sys.exit(1)

        mergeDicts(self._textOptions, self._newOptions)
        self._printOptionMap()
        self._setupText()

        self._textPave.SetTextSize(      self._textOptions["textSize"])
        self._textPave.SetTextColorAlpha(self._textOptions["textColor"],
                                         self._textOptions["textAlpha"])
        self._textPave.SetTextAngle(     self._textOptions["textAngle"])
        self._textPave.SetTextAlign(12)

        for line in ["textLine", "textLine2", "textLine3", "textLine4", "textLine5"]:
            if self._textOptions[line] == "": continue
            self._textPave.AddText(self._textOptions[line])

        logger.debug("Formatted text %s", self._name)
        return


