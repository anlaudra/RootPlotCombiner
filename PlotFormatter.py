#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date Nov. 2018
@file PlotFormatter.py
@brief Definition of the PlotFormatter class and of its options.
"""

from __future__ import print_function, division, absolute_import, unicode_literals

import sys
import logging
from collections import OrderedDict
#from ROOT import TH1F, TH2F, TProfile

from Helpers import mergeDicts
from logger_cfg import getLogger
logger = getLogger(__name__)


#_______________________________________________________________________________
def defaultPlotOptionMap():
    """Create the default option map."""
    plotOptions = OrderedDict({})
    plotOptions["plotOn"]       = "main"
    plotOptions["histMain"]     = ""
    plotOptions["histSub"]      = ""
    plotOptions["histDiv"]      = ""
    plotOptions["shiftX"]       = 0. # this histogram will be shifted by this value toward the right
    plotOptions["drawOpt"]      = ""
    plotOptions["legendEntry"]  = ""
    plotOptions["legendType"]   = "LP"
    plotOptions["lineColor"]    = 1
    plotOptions["lineStyle"]    = 1
    plotOptions["lineWidth"]    = 1 # short int, mine 1, ATLAS default 2
    plotOptions["fillColor"]    = 0
    plotOptions["fillStyle"]    = 0
    plotOptions["fillAlpha"]    = 1.
    plotOptions["markerColor"]  = 1
    plotOptions["markerSize"]   = 1 # mine 1, ATLAS default 1.2
    plotOptions["markerStyle"]  = 1
    return plotOptions



#_______________________________________________________________________________
class PlotFormatter(object):
    """Given a block of options and the list of available hists, fill
        the option map in and setup the histo.

    WARNING: Make sure to properly type the variables in the template
    maps (used to cast the string of the option reader).
    See https://root.cern.ch/doc/master/RtypesCore_8h_source.html
    Configuration: `doDebug`, `name`, `plotOptions`, `availableHists`.
    Main callable: `formatPlot()`.
    Deliverable: `hist`.
    1.  Check that the provided section is a plot formatting.
    2.  Setup the correct name for the formatter.
    3.  Retrieve all necessary `hist`s from `availableHists`
        and perform subtraction and division if required.
    4.  Apply the style described in the option map, and shift if required.
    """

    def __init__(self, name, newOptions, availableHists):
        """
        @param[in] name: section header.
        @param[in] newOptions: (unordered) dict of options
            that have to be changed.
        @param[in] availableHists: dictionnary of HistBuilders
            previously built.
        """
        self.doDebug = False
        self._name = name
        self._newOptions = newOptions
        self._plotOptions = defaultPlotOptionMap()
        self._availableHists = availableHists
        self._hist = None

    @property
    def nameNoOption(self):
        """Actual name of the instance: section name without leading options."""
        return self._name.replace("%plot%", "")

    @property
    def plotOptions(self):
        """Map of options provided (not updated before formatPlot is called)."""
        return self._plotOptions

    @property
    def hist(self):
        """Deliverable: the formatted histogram (after formatPlot is called)."""
        return self._hist


    #___________________________________________________________________________
    def _printOptionMap(self):
        """Print the map used for this histogram."""
        if not logger.isEnabledFor(logging.VERBOSE):
            return
        logger.verbose("Map content for plot: %s (%s)",
                       self.nameNoOption, self._name)
        for (key, value) in self._plotOptions.items():
            logger.verbose("%-15s : %s", key, value)
        return


    #___________________________________________________________________________
    def _checkAvailableHist(self, histName):
        """Check that the hist name provided exists in self._availableHists.
            Fails otherwise.

        @param[in] histName: name of the histogram to check.
        """
        if not histName in self._availableHists:
            logger.error("%s not available in:", histName)
            logger.error(sorted(self._availableHists.keys()))
            logger.error("Will now exit.")
            sys.exit(1)


    #___________________________________________________________________________
    def _setupHist(self):
        """Perform the Add/Divide if needed."""

        # Clone the input hist.
        # The output name is the same, with prefix input_ removed.
        logger.debug("Setting up histogram...")
        histMainName = "input_" + self._plotOptions["histMain"]
        self._checkAvailableHist(histMainName)
        self._hist = self._availableHists[histMainName].hist.Clone(self.nameNoOption)
        if not self._hist:
            logger.error("Could not retrieve histMain: %s", histMainName)
            logger.error("Will now exit.")
            sys.exit(1)

        if self._plotOptions["histSub"]:
            logger.debug("Provided histSub: %s", self._plotOptions["histSub"])
            histSubName = "input_" + self._plotOptions["histSub"]
            self._checkAvailableHist(histSubName)
            histSub = self._availableHists[histSubName].hist
            if not histSub:
                logger.error("Could not retrieve histSub: %s", histSubName)
                logger.error("Will now exit.")
                sys.exit(1)
            self._hist.Add(histSub, -1)
        else:
            logger.debug("No 'sub' hist provided: not substracting from the "
                         "nominal histogram.")

        if self._plotOptions["histDiv"]:
            logger.debug("Provided histDiv: %s", self._plotOptions["histDiv"])
            histDivName = "input_" + self._plotOptions["histDiv"]
            self._checkAvailableHist(histDivName)
            histDiv = self._availableHists[histDivName].hist
            if not histDiv:
                logger.error("Could not retrieve histDiv: %s",
                             "input_" + self._plotOptions["histDiv"])
                logger.error("Will now exit.")
                sys.exit(1)
            self._hist.Divide(histDiv)
        else:
            logger.debug("No 'div' hist provided: not dividing the nominal "
                         "histogram.")

        logger.debug("Histogram set up.")
        self._hist.SetDirectory(0)
        return


    #___________________________________________________________________________
    def _applyStyle(self):
        """Apply style given by the options to this plot."""

        logger.debug("Applying style...")
        # Apply shift on X-axis.
        self._hist.GetXaxis().SetLimits(
            self._hist.GetXaxis().GetXmin() + self._plotOptions["shiftX"],
            self._hist.GetXaxis().GetXmax() + self._plotOptions["shiftX"])

        # Default fill style is 0 (do not fill). So if we really want to fill
        # it, we need to also put the fillStyle option to 1001. Boring...
        # If the fillColor is changed from 0, it means we want to fill it. In
        # such case, if the fillStyle was not changed, override it to 1001; if
        # it was changed (to 3XXX), keep it as it is.
        # If we want a solid white background, we just need to specify fillStyle
        # 1001, then nor the fill style or color we be overriden.
        fillStyle = self._plotOptions["fillStyle"]
        if fillStyle == 0 and self._plotOptions["fillColor"] != 0:
            # We want to fill with other than white (default).
            fillStyle = 1001

        # Apply style options.
        self._hist.SetLineColor(     self._plotOptions["lineColor"])
        self._hist.SetLineStyle(     self._plotOptions["lineStyle"])
        self._hist.SetLineWidth(     self._plotOptions["lineWidth"])
        self._hist.SetFillColorAlpha(self._plotOptions["fillColor"],
                                     self._plotOptions["fillAlpha"])
        self._hist.SetFillStyle(     fillStyle)
        self._hist.SetMarkerColor(   self._plotOptions["markerColor"])
        self._hist.SetMarkerSize(    self._plotOptions["markerSize"])
        self._hist.SetMarkerStyle(   self._plotOptions["markerStyle"])

        logger.debug("Applied style.")
        return


    #___________________________________________________________________________
    def formatPlot(self):
        """Main callable."""

        logger.debug("Formatting plot %s", self._name)
        if not "%plot%" in self._name:
            logger.error("Trying to call PlotFormatter without %plot% in "
                         "section name.")
            logger.error("Will now exit.")
            sys.exit(1)

        mergeDicts(self._plotOptions, self._newOptions)
        self._printOptionMap()
        self._setupHist()
        self._applyStyle()
        logger.debug("Formatted plot %s", self._name)
        return

