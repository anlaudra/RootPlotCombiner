#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Antoine Laudrain <antoine.laudrain@cern.ch>
@date Jan. 2017 - Nov. 2018
@file HistBuilder.py
@brief Definition of the HistBuilder class, and of its options.
"""

from __future__ import print_function, division, absolute_import, unicode_literals

import sys
import logging
from array import array
from collections import OrderedDict
from ROOT import TFile, TChain, TH1F, TH2F, TProfile #, TTree

from Helpers import stringToList, mergeDicts
from logger_cfg import getLogger
logger = getLogger(__name__)


#_______________________________________________________________________________
def defaultBuilderOptionMap():
    """Create the default option map."""
    #pylint: disable=line-too-long
    histOptions = OrderedDict({})
    histOptions["inputFile"]    = ""
    histOptions["inputHist"]    = "" # used if the histogram has to be read from the file
    histOptions["inputTree"]    = "" # used if the histogram has to be projected
    histOptions["projType"]     = "" # used if the histogram has to be projected
    histOptions["projVar"]      = "" # used if the histogram has to be projected
    histOptions["cutVar"]       = "" # used if the histogram has to be projected
    histOptions["binningX"]     = "" # used if the histogram has to be projected
    histOptions["nBinsX"]       = 100 # used if the histogram has to be projected
    histOptions["xmin"]         = 0. # used if the histogram has to be projected
    histOptions["xmax"]         = 1. # used if the histogram has to be projected
    histOptions["binningY"]     = "" # used if the histogram has to be projected
    histOptions["nBinsY"]       = 100 # used if the histogram has to be projected in 2D case
    histOptions["ymin"]         = 0. # used if the histogram has to be projected in 2D case
    histOptions["ymax"]         = 1. # used if the histogram has to be projected in 2D case
    histOptions["normalize"]    = False
    return histOptions



#_______________________________________________________________________________
class HistBuilder(object):
    """Given a block of options, fill the option map in and setup an
        input histo.

    WARNING: Make sure to properly type the variables in the template
    maps (used to cast the string of the option reader).
    See https://root.cern.ch/doc/master/RtypesCore_8h_source.html
    Configuration: `doDebug`, `name`, `histOptions`.
    Main callable: `buildHistogram()`.
    Deliverable: `hist`.
    1.  Check that the provided section is a hist building.
    2.  Setup the correct name for the builder.
    3.  Setup internal variables controlling the type of projection
        (would that be needed in the following).
    4.  Open the input file describe in the options.
    5.  Try to retrieve an existing histogram.
    6.  If failed (if `inputHist` not specified), try to retrieve the
        tree and project from it.
    7.  Prepend the built histogram name with 'input_' to avoid
        conflict when formatting.
        Ownership of this histogram given to user.
    8.  Normalize histogram if required.
    """

    def __init__(self, name, newOptions):
        """
        @param[in] name: section header.
        @param[in] newOptions: (unordered) dict of options that have
            to be changed.
        """
        self.doDebug = False
        self._name = name
        self._isProj1D = False
        self._isProj2D = False
        self._isProjProfile = False
        self._newOptions = newOptions
        self._histOptions = defaultBuilderOptionMap()
        self._hist = None

    @property
    def nameNoOption(self):
        """Actual name of the instance: section name without leading
            options.
        """
        return self._name.replace("%hist%", "")

    @property
    def histOptions(self):
        """Map of options provided (not updated before buildHistogram
            is called).
        """
        return self._histOptions

    @property
    def hist(self):
        """Deliverable: the formatted histogram (after buildHistogram
            is called).
        """
        return self._hist


    #___________________________________________________________________________
    def _printOptionMap(self):
        """Print the map used for this histogram."""
        if not logger.isEnabledFor(logging.VERBOSE):
            return
        logger.verbose("Map content for hist: %s (%s)",
                       self.nameNoOption, self._name)
        for (key, value) in self._histOptions.items():
            logger.verbose("%-15s : %s", key, value)
        return


    #___________________________________________________________________________
    def _retrieveHistFromFile(self, inFilePathList):
        """Try to retrieve the histogram with name given in option from a file.

        @param[in] inFilePathList: list of (properly formatted) paths,
            only the first is taken.

        @return: boolean corresponding to success or not.
        """
        if len(inFilePathList) != 1:
            logger.warning("Provided more than one file path, taking first.")
        inFilePath = inFilePathList[0]

        inFile = TFile.Open(inFilePath)
        if not inFile or not inFile.IsOpen():
            logger.error("Failed opening file %s", inFilePath)
            logger.error("Will now exit.")
            sys.exit(1)
        logger.debug("Opened input file: %s", inFilePath)

        logger.info("Trying to retrieve histogram from file: %s",
                    inFile.GetName())
        if not self._histOptions["inputHist"]:
            logger.debug("'inputHist' option not provided, "
                         "skipping this method.")
            inFile.Close()
            del inFile
            return False

        self._hist = inFile.Get(bytes(self._histOptions["inputHist"]))
        if not self._hist:
            logger.error("Could not retrieve histogram: %s",
                         self._histOptions["inputHist"])
            logger.error("Will now exit.")
            sys.exit(1)
        logger.info("Successfully retrieved hist: %s",
                    self._histOptions["inputHist"])

        self._hist.SetDirectory(0) # take ownership
        inFile.Close()
        del inFile
        return True


    #___________________________________________________________________________
    def _buildBinning(self):
        """Build the 1D and 2D hist constructor args.

        @return: 1D and 2D constructor args
        """
        logger.debug("Building histogram binning...")
        # Build constructor of TH1F.
        # The parameters are put in a list which will be expanded in the TH1
        # ctor call.
        # Choose between building from list of bins or boundaries.
        binningX = stringToList(self._histOptions["binningX"], typ=float)
        ctor1D = [self.nameNoOption, self.nameNoOption]
        if binningX:
            logger.debug("x-binning has explicit binning.")
            ctor1D += [len(binningX)-1, array('d', binningX)]
        else:
            logger.debug("x-binning from nbins/xmin/xmax.")
            ctor1D += [
                self._histOptions["nBinsX"],
                self._histOptions["xmin"],
                self._histOptions["xmax"]
            ]
        logger.debug("1D ctor: %s", ctor1D)

        # And add 2D part if needed, with same logic.
        binningY = stringToList(self._histOptions["binningY"], typ=float)
        ctor2D = []
        if binningY:
            logger.debug("y-binning has explicit binning.")
            ctor2D += [len(binningY)-1, array('d', binningY)]
        else:
            logger.debug("y-binning from nbins/xmin/xmax.")
            ctor2D += [
                self._histOptions["nBinsY"],
                self._histOptions["ymin"],
                self._histOptions["ymax"]
            ]
        logger.debug("2D ctor: %s", ctor2D)

        return ctor1D, ctor2D


    #___________________________________________________________________________
    def _buildHistFromTree(self, inFilePathList):
        """Try to project from a TChain, given the options in the map

        @param[in] inFilePathList: list of (properly formatted) path
            to build a TChain.

        @return: boolean corresponding to success or not.
            If an error occurs, still exits.
        """
        if not self._histOptions["inputTree"]:
            logger.debug("'inputTree' option not provided, "
                         "skipping this method.")
            return False

        chain = TChain(self._histOptions["inputTree"],
                       self._histOptions["inputTree"])
        for token in inFilePathList:
            nAdded = chain.Add(token)
            logger.info("Added %d trees from path %s", nAdded, token)
            if nAdded == 0:
                logger.warning("No tree added from previous path.")

        ctor1D, ctor2D = self._buildBinning()

        # Make sure this choice is unique
        if not (self._isProj1D ^ self._isProj2D):
            logger.error("None of (or both) 1D and 2D recognized in option: %s",
                         self._histOptions["projType"])
            logger.error("Will now exit.")
            sys.exit(1)

        # Check that projVar exists.
        if not self._histOptions["projVar"]:
            logger.error("inputTree provided, but not projVar...")
            logger.error("Will now exit...")
            sys.exit(1)
        logger.debug("Using 'projVar': %s", self._histOptions["projVar"])

        # Now project the tree.

        if self._isProj1D and self._isProjProfile:
            logger.debug("Config file has provided: Proj, 1D, profile")
            self._hist = TProfile(*ctor1D)
            chain.Project(self.nameNoOption,
                          self._histOptions["projVar"],
                          self._histOptions["cutVar"])
        elif self._isProj1D:
            logger.debug("Config file has provided: Proj, 1D")
            self._hist = TH1F(*ctor1D)
            chain.Project(self.nameNoOption,
                          self._histOptions["projVar"],
                          self._histOptions["cutVar"])
        elif self._isProj2D:
            logger.debug("Config file has provided: Proj, 2D")
            self._hist = TH2F(*(ctor1D + ctor2D))
            chain.Project(self.nameNoOption,
                          self._histOptions["projVar"],
                          self._histOptions["cutVar"])
        else:
            logger.error("Unrecognized histogram projection type: failure.")
            logger.error("Will now exit.")
            sys.exit(1)

        logger.debug("Successfully projected from tree.")
        self._hist.SetDirectory(0) # take ownership
        return True


    #___________________________________________________________________________
    def buildHistogram(self):
        """Main callable: opens the file to get the histogram described
            by the options.
        """
        logger.debug("Building histogram %s", self._name)
        if not "%hist%" in self._name:
            logger.error("Trying to call HistBuilder without %hist% in section "
                         "name.")
            logger.error("Will now exit.")
            sys.exit(1)

        # First, fill the option map and setup internal variables.
        mergeDicts(self._histOptions, self._newOptions)
        self._printOptionMap()

        self._isProj1D       = "1d" in self._histOptions["projType"].lower()
        self._isProj2D       = "2d" in self._histOptions["projType"].lower()
        self._isProjProfile  = "prof" in self._histOptions["projType"].lower()
        logger.debug("Received projType = %s -> "
                     "isProj1D: %d / isProj2D: %d / isProjProfile: %d",
                     self._histOptions["projType"],
                     self._isProj1D, self._isProj2D, self._isProjProfile)

        # Then retrieve the histogram
        inFilePathList = stringToList(self._histOptions["inputFile"])
        for token in inFilePathList:
            logger.debug("File token: %s", token)

        # First, check if we can retrieve the hist from a file.
        histIsReady = False
        if not self._histOptions["projType"]:
            logger.debug("No 'projType' provided, retrieving hist from file.")
            histIsReady = self._retrieveHistFromFile(inFilePathList)
        else:
            logger.debug("'projType' provided, building hist from tree.")
            histIsReady = self._buildHistFromTree(inFilePathList)

        if not histIsReady:
            logger.error("Both retrieve and project failed. Hist not ready.")
            logger.error("Will now exit.")
            sys.exit(1)

        # Rename to avoid conflicts.
        self._hist.SetName("input_" + self.nameNoOption)

        # Normalize if required.
        if self._histOptions["normalize"]:
            norm = self._hist.GetSumOfWeights()
            logger.debug("Normalising histogram with SumOfWeights = %s", norm)
            if norm != 0:
                self._hist.Scale(1./norm)
            else:
                logger.warning("Norm in range is 0!!! - Not normalising.")

        logger.debug("Built histogram %s", self._name)
        return

